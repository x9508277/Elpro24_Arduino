#ifndef Database_h
#define Database_h

#pragma once

#include "Arduino.h"
#include <SPI.h>
#include "SdFat.h"
#include "Worker.h"
#include "Passage.h"
#include "CharsHelper.h"

class Database{
    public:
        Database(uint8_t chipSelect, int speed);
        SdFile file;
        SdFatSoftSpi<7, 6, 5> sdSoft;
        char* getSkipDir();
        char* getPassDir();
        char* getRootDir();
        bool begin();
        bool checkDirectory(char* dir);
        bool createDirectory(char* dir);
        bool dirCreateIfNotExists(char* dir);
        int getPassagesCount();
        Passage** getPassages();
        bool removeSkips();
        bool removePassages();
        bool searchSkip(char* code);
        char* getSkipIdByCode(char* code);
        bool AddSkip(Worker* worker);
        bool AddSkip(char* id, char* code);
        bool AddPassage(Passage* passage);
        bool AddPassage(char* id, char* date);
        char* getSerializedPassages(int count, int passageLength);
        void removePassagesByCount(int count);

    private:
        uint8_t chipSelect;
        int speed;
        char* skipDirectory;
        char* passDirectory;
        CharsHelper ch;
};

#endif