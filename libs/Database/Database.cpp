#include "Arduino.h"
#include "Database.h"

#define DEBUG 1

#if DEBUG
#include <MemoryFree.h>
#include <pgmStrToRAM.h>
#endif

Database::Database(uint8_t chipSelect, int speed){
    this->chipSelect = chipSelect;
    this->speed = speed;
    passDirectory = (char*)"Pass/";
    skipDirectory = (char*)"Skips/";
}

char* Database::getSkipDir(){
    return skipDirectory;
}

char* Database::getPassDir(){
    return passDirectory;
}

char* Database::getRootDir(){
    return (char*)"/";
}

bool Database::begin(){
    return sdSoft.begin(chipSelect);
}

bool Database::checkDirectory(char* dir) {
    return sdSoft.exists(dir);
}

bool Database::createDirectory(char* dir) {
    return sdSoft.mkdir(dir);
}

bool Database::dirCreateIfNotExists(char* dir) {
    return checkDirectory(dir) ? true : createDirectory(dir);
}

int Database::getPassagesCount() {
    int count = 0;
    sdSoft.chdir("/");
    sdSoft.chdir("/Pass/");
    sdSoft.vwd() -> rewind();

    while(file.openNext(sdSoft.vwd(), O_READ)){
        if (!file.isDir())
            count++;
        file.close();
    }

    sdSoft.chdir("/");
    return count;
}

Passage** Database::getPassages() {
    int i = 0;
    int n = getPassagesCount();
    Passage** passages = new Passage*[n];
    sdSoft.chdir("/Pass/");
    sdSoft.vwd()->rewind();

    while(file.openNext(sdSoft.vwd(), O_READ)) {
        if (!file.isHidden() && !file.isDir()) {
            char* temp = new char[13];
            file.getName(temp, 13);
            ch.strTrim(temp);
            ch.strRemove(temp, '\n');
            char* tempId = new char[4];
            file.fgets(tempId, 4);
            ch.strTrim(tempId);
            ch.strRemove(tempId, '\n');
            passages[i] = new Passage(tempId, temp);
            //passages[i] = Passage(tempId, temp);
            //passages[i]->setId(tempId);
            //passages[i]->setDate(temp);
            i++;
        }
        file.close();
    }

    sdSoft.chdir("/");
    return passages;    
}

bool Database::removeSkips() {
    return file.open("Skips") ? file.rmRfStar() : false;
}

bool Database::removePassages() {
    return file.open("Pass") ? file.rmRfStar() : false;
}

bool Database::searchSkip(char* code) {
    sdSoft.chdir("/");
    char* passagePath = ch.strConcat(skipDirectory, code);

    #if DEBUG
    Serial.println(strlen(passagePath));
    #endif
    
    bool result = file.open(passagePath);
    file.close();
    sdSoft.chdir("/");

    delete[] passagePath;
    return result;
}

char* Database::getSkipIdByCode(char* code){
    sdSoft.chdir("/");
    char* tempId = new char[9];     
    char* passagePath = ch.strConcat(skipDirectory, code);
    if (!file.open(passagePath)) {
        delete[] passagePath;
        tempId[0] = '`';
        file.close();
        return tempId;
    }

    if (file.fgets(tempId, sizeof(char) * 9) <= 0) {
        delete[] passagePath;
        tempId[0] = '`';
        file.close();
        return tempId;
    }

    ch.strTrim(tempId);
    ch.strRemove(tempId, '\n'); 
    file.close();
    sdSoft.chdir("/");

    delete[] passagePath;
    return tempId;
}

bool Database::AddSkip(Worker* worker) {
    sdSoft.chdir("/");
    char* path = ch.strConcat(skipDirectory, worker->getCode());
    if (!file.open(path, O_CREAT | O_WRITE)) {
        delete[] path;
        file.close();
        return false;
    }

    char* id = worker->getId();
    ch.strTrim(id);
    ch.strRemove(id, '\n');
    file.println(id);
    file.close();

    delete[] path;
    sdSoft.chdir("/");
    return true;
}

bool Database::AddSkip(char* id, char* code){
    sdSoft.chdir("/");
    char* path = ch.strConcat(skipDirectory, code);
    Serial.println(path);
    if (!file.open(path, O_CREAT | O_WRITE)) {
    	#if DEBUG
    	Serial.println(F("er2"));
    	#endif
        delete[] path;
        file.close();
        return false;
    }

    file.remove();
    file.close();

    if (!file.open(path, O_CREAT | O_WRITE)) {
    	#if DEBUG
    	Serial.println(F("er2"));
    	#endif
        delete[] path;
        file.close();
        return false;
    }

    ch.strTrim(id);
    ch.strRemove(id, '\n');
    file.print(id);
    file.close();

    delete[] path;
    delete[] id;
    delete[] code;
    sdSoft.chdir("/");
    return true;
}

bool Database::AddPassage(char* id, char* date){
    sdSoft.chdir("/");
    char* path = ch.strConcat(passDirectory, date);

    #if DEBUG
    Serial.println(path);
    Serial.println(id);
    #endif

    if (!file.open(path, O_CREAT | O_WRITE)) {
        #if DEBUG
        Serial.println(F("Error"));
        #endif
        file.close();
        delete[] path;
        return false;
    }

    ch.strTrim(id);
    ch.strRemove(id, '\n');
    file.print(id);
    file.close();

    delete[] path;
    sdSoft.chdir("/");
    return true;
}

bool Database::AddPassage(Passage* passage){
    sdSoft.chdir("/");
    char* path = ch.strConcat(passDirectory, passage->getDate());

    #if DEBUG
    Serial.println(path);
    #endif

    if (!file.open(path, O_CREAT | O_WRITE)) {
        #if DEBUG
        Serial.println(F("Error"));
        #endif
        file.close();
        delete[] path;
        return false;
    }

    file.remove();

    if (!file.open(path, O_CREAT | O_WRITE)) {
        #if DEBUG
        Serial.println(F("Error"));
        #endif
        file.close();
        delete[] path;
        return false;
    }

    char* id = passage->getId();
    ch.strTrim(id);
    ch.strRemove(id, '\n');
    file.print(id);
    file.close();

    delete[] path;
    delete[] id;
    sdSoft.chdir("/");
    return true;
}

char* Database::getSerializedPassages(int count, int passageLength){
    int i = 1;
    int n = getPassagesCount();

    if (count > n)
        count = n;

    //Passage** passages = new Passage*[count];
    sdSoft.chdir("/Pass/");
    sdSoft.vwd()->rewind();
    short len = count * passageLength + 3;
    char* result = new char[len];
    int templen = 0;
    result[0] = 'p';
    
    for (int j = 0; j < count; j++) {    
        if (!file.openNext(sdSoft.vwd(), O_READ)) {
            #if DEBUG
            Serial.println(F("Error"));
            #endif
            continue;
        }

        if (/*!file.isHidden() && */!file.isDir()){
            char* temp = new char[13];
            file.getName(temp, 13);
            ch.strTrim(temp);
            ch.strRemove(temp, '\n');

            #if DEBUG
            Serial.println(temp);
            #endif

            templen = strlen(temp);
            for (int l = 0; l < templen; l++){
                result[i] = temp[l];
                i++;
            }

            char* tempId = new char[9];

            #if DEBUG
            Serial.println(tempId);
            #endif

            file.fgets(tempId, 9);
            ch.strTrim(tempId);
            ch.strRemove(tempId, '\n');

            templen = strlen(tempId);
            for (int l = 0; l < templen; l++){
                result[i] = tempId[l];
                i++;
            }

            delete[] temp;
            delete[] tempId;
        }   
        else {
        	#if DEBUG
            Serial.println(F("hidden"));
            #endif
        }
        file.close();
    } 

    result[i++] = ';';
    result[i] = 0;

    sdSoft.chdir("/");
    return result; 
}

void Database::removePassagesByCount(int count){
    sdSoft.chdir("/Pass/");
    sdSoft.vwd()->rewind();

    for (int i = 0; i < count; i++){
        if (file.openNext(sdSoft.vwd(), O_READ | O_WRITE))
            file.remove();
    }
}