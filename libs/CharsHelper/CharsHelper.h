#ifndef CharsHelper_h
#define CharsHelper_h

#pragma once

#include "Arduino.h"

class CharsHelper{
    public:
        void strTrim(char *chars);
        void strReplace(char *chars, char oldChar, char newChar);
        void strRemove(char *chars, char removeChar);
        char* strConcat(char* firstChars, char* secondChars);
        char* strSubstring(char* str, short x, short y);
        char* toDigits(char* str);
};

#endif