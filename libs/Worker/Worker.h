#ifndef Worker_h
#define Worker_h

#pragma once

#include "Arduino.h"

class Worker{
    public:
        Worker();
        Worker(char* skip_id);
        Worker(char* skip_id, char* code);
        //rker(const char* skip_id, const char* code);
        ~Worker();
        char* getId();
        char* getCode();
        void setId(char* skip_id);
        void setCode(char* code);
    
    private:
        char* _skip_id;
        char* _code;
};

#endif