#include "Arduino.h"
#include "Worker.h"

Worker::Worker() : Worker(NULL, NULL) {
}

Worker::Worker(char* skip_id) : Worker(skip_id, NULL) {
}

Worker::Worker(char* skip_id, char* code) {
    _skip_id = skip_id;
    _code = code;
}

Worker::~Worker() {
    if (_skip_id != NULL)
        delete[] _skip_id;
    if (_code != NULL)
        delete[] _code;
}

char* Worker::getId() {
    return _skip_id;
}

char* Worker::getCode() {
    return _code;
}

void Worker::setId(char* skip_id) {
    _skip_id = skip_id;
}

void Worker::setCode(char* code) {
    _code = code;
}