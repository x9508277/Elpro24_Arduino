#include <avr/wdt.h>
#include <Worker.h>
#include <Database.h>
#include <Passage.h>
#include <SoftwareSerial.h>
#include <CharsHelper.h>
#include <SoftSerialEx.h>
#include <Wiegand.h>
#include <iarduino_RTC.h>
#include "timer-api.h"

#define DEBUG 1

#if DEBUG
#include <MemoryFree.h>
#include <pgmStrToRAM.h>
#endif

char skipsDir[] = "Skips";
char passDir[] = "Pass";
String id = "arduinotest";
boolean isPress = false, isCard = false;
short timeId = 48;

Database* db = new Database(10, 50);
SoftSerialEx soft;

CharsHelper ch;
WIEGAND wg;
iarduino_RTC time(RTC_DS1302,4,12,13);

char s[] = "s", g[] = "g", r[] = "r", t[] = "t";

void setup() {
  Serial.begin(9600);
  wg.begin();

  pinMode(13, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(A1, INPUT);
  digitalWrite(11, HIGH);
  time.begin();
  //settime (sec, min, hours, d, m, y)
  //time.settime(0, 44, 15, 19, 7, 18, 4);
  #if DEBUG
  Serial.println(getTime());
  #endif
  //start watchdog
  wdt_enable(WDTO_8S);
  
  //waiting for SD...
  while (!db->begin()) {
    wdt_reset();

    #if DEBUG
    Serial.println(F("Cannot open SD card"));
    #endif

    delay(1000);
  }

  //db->removePassagesByCount(5);

  #if DEBUG
  Serial.println(F("Ready"));
  #endif
  
  //check directoies
  db->dirCreateIfNotExists(skipsDir);
  db->dirCreateIfNotExists(passDir);

  #if DEBUG
  Serial.println(freeMemory());
  #endif

  soft.send(r);

  //waiting for SoftwareSerial...
  while (!soft.available()) {
    wdt_reset();
    checkButton();
    if (Serial.available()){
      char c = Serial.read();
      Serial.readString();
      if (c == 'c')
        setTime();
    }
  }

  //Ready
  char c = soft.read();
  soft.send(t);

  #if DEBUG
  Serial.println(F("Ok"));
  #endif

  //lblink();
}

void loop() {
  wdt_reset();
  checkButton();
  #if DEBUG
  //Serial.println(freeMemory());
  //delay(1000);
  //Serial.println(F("loop"));

  #endif

  //check inputs from wiegand 26
  if (wg.available()){
    
    unsigned long tempCode = wg.getCode();
    char code[1 + 8 * sizeof(unsigned long)];
    ultoa(tempCode, code, HEX);    

    /*String tempCode = Serial.readString();
    int n = tempCode.length() + 1;
    char* code = new char[n];
    tempCode.toCharArray(code, n);
    */
    
    /*#if DEBUG
    Serial.println(tempCode);
    Serial.println(freeMemory());
    #endif

    #if DEBUG
    Serial.println(freeMemory());
    #endif*/

    if (db->searchSkip(code)){
      parseCode(code);      
    }  
    //delete[] code;  
    return;
  }

  #if DEBUG
  //check inputs from Serial port
  if (Serial.available()){
    /*char c = Serial.read();
    if (c == 'c')
      setTime();
      */
    //delay(100);
    String tempCode = Serial.readString();
    int n = tempCode.length() + 1;
    char* code = new char[n];
    tempCode.toCharArray(code, n);
    
    //char* code = "bee340";
    /*Serial.readString();
    unsigned long tempCode = 12510016;
    char code[1 + 8 * sizeof(unsigned long)];
    ultoa(tempCode, code, HEX);
    */
    
    parseCode(code);
    //delete[] code;
    return;
  }
  #endif

  //check inputs from SoftwareSerial port
  if(!soft.available()) return;
  
  #if DEBUG
  Serial.println(F("Run"));
  lblink();
  #endif
  

  char c = soft.read();
  #if DEBUG
  Serial.println(c);
  #endif
  switch(c){
    case 't':{
      int times[7];
      char s[3];
      s[0] = soft.read();
      s[1] = soft.read();
      s[2] = 0;
      times[0] = atoi(s);
      //Serial.println(times[0]);
      wdt_reset();

      char m[3];
      m[0] = soft.read();
      m[1] = soft.read();
      m[2] = 0;
      times[1] = atoi(m);
      //Serial.println(times[1]);
      wdt_reset();

      char h[3];
      h[0] = soft.read();
      h[1] = soft.read();
      h[2] = 0;
      times[2] = atoi(h);
      //Serial.println(times[2]);
      wdt_reset();

      char d[3];
      d[0] = soft.read();
      d[1] = soft.read();
      d[2] = 0;
      times[3] = atoi(d);
      //Serial.println(times[3]);
      wdt_reset();

      char i[3];
      i[0] = soft.read();
      i[1] = soft.read();
      i[2] = 0;
      times[4] = atoi(i);
      //Serial.println(times[4]);
      wdt_reset();

      char y[3];
      y[0] = soft.read();
      y[1] = soft.read();
      y[2] = 0;
      times[5] = atoi(y);
      //Serial.println(times[5]);
      wdt_reset();

      char dOY[3];
      dOY[0] = soft.read();
      dOY[1] = soft.read();
      dOY[2] = 0;
      times[6] = atoi(dOY);
      //Serial.println(times[6]);
      wdt_reset();

      time.settime(times[0], times[1], times[2], times[3], times[4], times[5], times[6]);    
      //Serial.println(getTime());
      wdt_reset();

      soft.send(r);     
      break;
    }
    case 's':{
      getSkips();
      soft.send(g);
      break;
    }
    case 'e':{
      /*#if DEBUG
      lblink();
      delay(500);
      lblink();
      #endif*/

      short count = db->getPassagesCount();
      if (count > 0){
        getAndSendPassages();
        Serial.println(count);
      }
        
      break;
    }
    case 'd':{
      #if DEBUG
      Serial.println(F("d"));
      #endif

      db->removePassagesByCount(5);
      
      short count = db->getPassagesCount();
      if (count > 0){
        getAndSendPassages();
        Serial.println(count);
      }
/*
      unsigned long tempCode = 12510016;
      char code[1 + 8 * sizeof(unsigned long)];
      ultoa(tempCode, code, HEX);
    
    
      parseCode(code);
        */
      //isCard = true;
      //openDoor();
      break;
    }      
    default:{
      soft.send(s);

      #if DEBUG
      Serial.println(F("Default"));
      #endif

      break;
    }
  }
  #if DEBUG
  Serial.println(freeMemory());
  #endif
}

void setTime(){
  int sec,mins,hour,d,i,y,dOW;
  Serial.println(F("sec"));
  while(!Serial.available()){wdt_reset();}
   sec = Serial.parseInt();
  Serial.println(sec);

  Serial.println(F("min"));
  while(!Serial.available()){wdt_reset();}
  mins = Serial.parseInt();
  Serial.println(mins);

  Serial.println(F("hour"));
  while(!Serial.available()){wdt_reset();}
  hour = Serial.parseInt();
  Serial.println(hour);

  Serial.println(F("d"));
  while(!Serial.available()){wdt_reset();}
  d = Serial.parseInt();
  Serial.println(d);

  Serial.println(F("i"));
  while(!Serial.available()){wdt_reset();}
  i = Serial.parseInt();
  Serial.println(i);

  Serial.println(F("y"));
  while(!Serial.available()){wdt_reset();}
  y = Serial.parseInt();
  Serial.println(y);

  Serial.println(F("dOW"));
  while(!Serial.available()){wdt_reset();}
  dOW = Serial.parseInt();
  Serial.println(dOW);

  //settime (sec, min, hours, d, m, y, day of week)
  time.settime(sec, mins, hour, d, i, y, dOW);    
  Serial.println(getTime());
}

char* appendCharToCharArray(char* array, char a){
  size_t len = strlen(array);

  char* ret = new char[len+2];

  strcpy(ret, array);    
  ret[len] = a;
  ret[len+1] = '\0';

  return ret;
}

//looking for a skip on the SD and creating a passage
void parseCode(char* code){
  char* id = db->getSkipIdByCode(code);

  /*#if DEBUG
  Serial.println(freeMemory());
  Serial.println(id);
  #endif*/

  if (id[0] != '`'){
/*
    #if DEBUG    
    Serial.println(freeMemory());
    #endif
*/
    char index = timeId++;
    char* date = appendCharToCharArray(getTime(), index);
    //Serial.println(date);
    if (timeId >= 57)
      timeId = 48;
    
    db->AddPassage(id, date);


    if (!isPress){
      isCard = true;
      openDoor();
    }
      
    delete[] date;
  }
  /*#if DEBUG
  Serial.println(freeMemory());
  #endif*/

  delete[] id;
}

//get first 5 passages from SD and writes to SoftwareSerial port
void getAndSendPassages(){
  char* passages = db->getSerializedPassages(5, 20);
  #if DEBUG
  Serial.println(passages);
  Serial.println(freeMemory());
  #endif
  
  soft.println(passages);
  delete[] passages;
}

//get all skips from SoftwareSerial port (Elpro24_Slave) and writes to a SD 
void getSkips(){
  int n = 0;
  #if DEBUG
  Serial.println(freeMemory());
  #endif
  char* buf = soft.getSerialData(n);
  int count = n / 14;
  
  #if DEBUG
  Serial.println(F("Serial"));
  Serial.println(freeMemory());
  #endif
  
  soft.parseAndCreateSkips(buf, 14, count, db);

  #if DEBUG
  Serial.println(freeMemory());
  #endif
}

//returns the current time in the format ('dmyHis')
char* getTime(){
  return time.gettime("dmyHis");
}

//checks if the open button is pressed
void checkButton(){
  if (!isPress && digitalRead(11) == LOW){
    openDoor();
  }
}

//Set LOW on relay pin (11) and start 5sec timer
void openDoor(){
  #if DEBUG
  Serial.println(F("bon"));
  #endif

  digitalWrite(11, LOW);
  isPress = true;    
  timer_init_ISR_1Hz(TIMER_DEFAULT);
}

//Blink on L led
void lblink(){
  digitalWrite(13, HIGH);
  delay(200);
  digitalWrite(13, LOW);
}

//timer function for the set HIGH on the relay pin (11) and disable timer
void timer_handle_interrupts(int timer){
  static int count = 4;

  if (isCard){
    #if DEBUG
    Serial.println(F("bof"));
    #endif
    
    digitalWrite(11, HIGH);
    isPress = false;
    isCard = false;
    timer_stop_ISR(TIMER_DEFAULT);
    count = 4;
    return;
  }

  if (count == 0 && isPress){
    #if DEBUG
    Serial.println(F("bof"));
    #endif
    
    digitalWrite(11, HIGH);
    isPress = false;
    isCard = false;
    timer_stop_ISR(TIMER_DEFAULT);
    count = 4;
  }
  else
    count--;  
}


